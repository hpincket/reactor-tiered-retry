plugins {
    kotlin("jvm") version "1.8.0"
    application
}

group = "com.hpincket"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("io.projectreactor:reactor-core:3.5.3")
    testImplementation("io.projectreactor:reactor-test:3.5.3")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("MainKt")
}