import reactor.core.publisher.Mono
import java.time.Duration
import java.util.concurrent.TimeoutException


class TieredTimeoutRetry(
  private val timeouts: List<Duration>,
  private val isRetryable: (Throwable) -> Boolean = { false }
) {

  fun <T> run(operation: Mono<T>): Mono<T> {
    return innerRun(operation, timeouts)
  }

  private fun <T> innerRun(operation: Mono<T>, remainingTimeouts: List<Duration>): Mono<T> {
    return when (remainingTimeouts.size) {
      0 -> operation
      1 -> {
        val nextTimeout = remainingTimeouts.first()
        operation.timeout(nextTimeout)
      }
      else -> {
        val nextTimeout = remainingTimeouts.first()
        operation
          .timeout(nextTimeout)
          .onErrorResume { t ->
            if (t is TimeoutException || isRetryable(t)) {
              innerRun(operation, remainingTimeouts.drop(1))
            } else {
              Mono.error(t)
            }
          }
      }
    }
  }

}