import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import util.VariableDelayer
import java.time.Duration
import java.util.concurrent.TimeoutException
import kotlin.IllegalArgumentException

class TieredTimeoutRetryTest {
  @Test
  fun `test mono result is returned`() {
    val ttr = TieredTimeoutRetry(listOf(Duration.ofSeconds(1)))
    StepVerifier.withVirtualTime {
      val m = Mono.just(42)
      ttr.run(m)
    }
      .expectNext(42)
      .verifyComplete()
  }

  @Test
  fun `test mono error is propagated`() {
    val ttr = TieredTimeoutRetry(listOf(Duration.ofSeconds(1)))
    StepVerifier.withVirtualTime {
      val m = Mono.error<Int>(IllegalArgumentException("This is a test"))
      ttr.run(m)
    }
      .verifyError(IllegalArgumentException::class.java)
  }

  @Test
  fun `test one timeout propagates error`() {
    val ttr = TieredTimeoutRetry(listOf(Duration.ofSeconds(1)))
    StepVerifier.withVirtualTime {
      val m = Mono
        .just(42)
        .delayElement(Duration.ofSeconds(2))
      ttr.run(m)
    }
      .thenAwait(Duration.ofSeconds(1))
      .verifyError(TimeoutException::class.java)
  }

  @Test
  fun `test multiple timeouts are retried`() {
    val ttr = TieredTimeoutRetry(listOf(
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(2)
    ))
    StepVerifier.withVirtualTime {
      val m = Mono
        .just(42)
        .delayElement(Duration.ofSeconds(2))
      ttr.run(m)
    }
      .thenAwait(Duration.ofSeconds(4))
      .expectNext(42)
      .verifyComplete()
  }

  @Test
  fun `test multiple short timeouts all fail`() {
    val ttr = TieredTimeoutRetry(listOf(
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
    ))
    StepVerifier.withVirtualTime {
      val m = Mono
        .just(42)
        .delayElement(Duration.ofSeconds(2))
      ttr.run(m)
    }
      .thenAwait(Duration.ofSeconds(7))
      .verifyError(TimeoutException::class.java)
  }

  @Test
  fun `test variable timeouts last gets it`() {
    val ttr = TieredTimeoutRetry(listOf(
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(5)
    ))
    StepVerifier.withVirtualTime {
      val m = VariableDelayer<Int>(42, listOf(
        Duration.ofSeconds(2),
        Duration.ofSeconds(3),
        Duration.ofSeconds(1),
      )).run()
      ttr.run(m)
    }
      .expectSubscription()
      .expectNoEvent(Duration.ofSeconds(3))
      .expectNext(42)
      .verifyComplete()
  }

  @Test
  fun `test variable timeouts second gets it`() {
    val ttr = TieredTimeoutRetry(listOf(
      Duration.ofSeconds(1),
      Duration.ofSeconds(1),
      Duration.ofSeconds(5)
    ))
    StepVerifier.withVirtualTime {
      val m = VariableDelayer<Int>(42, listOf(
        Duration.ofSeconds(2),
        Duration.ofSeconds(1),
        Duration.ofSeconds(5),
      )).run()
      ttr.run(m)
    }
      .expectSubscription()
      .expectNoEvent(Duration.ofSeconds(2))
      .expectNext(42)
      .verifyComplete()
  }

  @Disabled
  @Test
  fun `test retries different exceptions when specified`() {
    val ttr = TieredTimeoutRetry(listOf(
      Duration.ofSeconds(1),
      Duration.ofSeconds(1)
    )
    ) { it is java.lang.IllegalArgumentException }
    StepVerifier.withVirtualTime {
      ttr.run(Mono.just(42))
    }
  }
}