package util

import reactor.core.publisher.Mono
import java.time.Duration

class VariableDelayer<T>(
  private val element: T,
  private var delays: List<Duration>,
) {

  fun run(): Mono<T> {
    return Mono.just(element).flatMap {
      if (delays.isEmpty()) {
        Mono.just(it)
      } else {
        val delay = delays.first()
        this.delays = delays.drop(1)
        Mono.just(it).delayElement(delay)
      }
    }
  }
}